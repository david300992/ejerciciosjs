
        // variables globals, s'ompliran amb la informació procedent del JSON
        var marques = [];
        var motos = [];
        var cantidades = [];

        // quan tot està carregat, llegim JSON
        $(document).ready(function () {
            llegeixJSON();
        })

        // llegim del arxiu local, podriem fer-ho d'una ubicació remota amb la mateixa facilitat
        // ull Chrome no deixa fer això
        function llegeixJSON() {
            url = "file:///C:/Users/Programacion/Desktop/David%20Escudero/html/json/MOTOS_JSON/motos.json";
            
            $.getJSON(url, function (data) {
                //processem dades rebudes, omplint marques i motos
                processa(data);
                //filtrem per la marca de la primera moto
                filtra(motos[0].marca);
            });
        }

        // afegim event que filtrarà les motos quan es cliqui un botó
        $(document).on("click", "button.marca", function () {
            var marca = $(this).text(); 
            filtra(marca); //cridem funció filtra amb la marca d'aquest boto
        });


        // esborra taula (tbody) i fa un foreach per totes les motos buscant les de la  marca donada
        function filtra(marca) {
            esborraTaula();

            $("#nomMarca").text(marca); //establim el titol de la taula al nom de marca actual

            motos.forEach(function (moto) {
                //Si la marca de la moto es igual a la marca filtrada
                if (moto.marca === marca) {
                    //pinta afegirà la fila de la moto a la taula
                    pinta(moto);
                }
            });

        }

        function esborraTaula() {
            $("div#motos table tbody").empty();
        }

        function pinta(moto) {
            var row;

            /////////////////////////////////////////
            /////////////////////////////////////////
            // CREAR AQUI LA FILA PER AQUESTA MOTO //
            /////////////////////////////////////////
            /////////////////////////////////////////
            row = "<tr><td>"+moto.model+"</td><td>"+moto.eur+"</td><td>"+moto.km+"</td><td>"+moto.cc+"</td><td>"+moto.any+"</td></tr>";
            
            $("div#motos table tbody").append(row);
        }

      
        // aquesta funció crea els arrays "motos" i "marques" a partir de les dades
        // rebudes via ajax
       function processa(data) {
            // motos equival a les dades rebudes
            motos = data;
            // fem bucle per cada moto (similar for...length)
            motos.forEach(function (moto) {
                ////////////////////////////////////////////
                /////////////////////////////////////////
                // EN AQUEST BUCLE CAL OMPLIR L'ARRAI MARQUES
                // AMB LES MARQUES DE LES MOTOS; SENSE REPETIR-LES!
                if(marques.indexOf(moto.marca) ==-1){
                    marques.push(moto.marca)
                    cantidades.push(1)
                    //si la encuentras
                }else {
                    cantidades[marques.indexOf(moto.marca)]++
                }
                /////////////////////////////////////////
                /////////////////////////////////////////
            });
            // invoquem mostramarques per pintar el menu de marques
            mostraMarques();

        }

        //pinta el menu de marques, només un cop a l'inici
        //important: assigna al botó la classe "marca" i la data-marca
        function mostraMarques() {
            //Se vacia
            $("#marques").empty();
            
            
            marques.forEach(function (marca) {
                var nouBoto ='<button class="btn btn-secondary btn-block marca" type="button">';
                nouBoto = nouBoto+ marca+ '</button>';
                //CREAR AQUI EL BOTO A PARTIR DEL NOM DE LA MARCA
                $("#marques").append(nouBoto);
            });
            

            /*
            for (indice in marques){
                var nouBoto ='<button class="btn btn-secondary btn-block marca" type="button">';
                nouBoto = nouBoto+ marques[indice]+cantidades[indice]+ '</button>';
                $("#marques").append(nouBoto);
            }
            */
        }