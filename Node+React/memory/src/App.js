import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

//Importamos el componente Memory
import Memory from './components/Memory.js'

class App extends Component {
  render() {   
    return (
      <div className="App">
        <Memory />
      </div>
    );
  }
}

export default App;
