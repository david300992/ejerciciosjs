import React, { Component } from 'react';
//CSS que afecta al componente
import './Memory.css';

//Importar Button del reactstrap
import { Button, Navbar } from 'reactstrap';

import Marca from './Marca.js'

//El que exporta por defecto, los demas tendran que empezar con export
export default class Memory extends Component {

    constructor(props) {
        super(props);

        const LOGOS = [{
            img: "cocacola", isVisible: false, indice: 0
        },
        {
            img: "evian", isVisible: false, indice: 0
        },
        {
            img: "kitkat", isVisible: false, indice: 0
        },
        {
            img: "lipton", isVisible: false, indice: 0
        },
        {
            img: "maggi", isVisible: false, indice: 0
        },
        {
            img: "milka", isVisible: false, indice: 0
        },
        {
            img: "mms", isVisible: false, indice: 0
        },
        {
            img: "oldelpaso", isVisible: false, indice: 0
        },
        {
            img: "pepsi", isVisible: false, indice: 0
        },
        {
            img: "pringles", isVisible: false, indice: 0
        }]

        //Metodo que hara al clicar
        this.mezclar = this.mezclar.bind(this);
        //Metodo para mezclar las marcas
        this.shuffle = this.shuffle.bind(this);
        //Para saber si a clicado
        this.clicado = this.clicado.bind(this);

        //En el estado se ponen los logos dos veces, mezclados con shuffle
        this.state = { logos: this.shuffle(LOGOS.concat(LOGOS))  
        };
    }

    mezclar() {
        this.setState({
            logos: this.shuffle(this.state.logos)
        })
    }

    clicado(hash){
        console.log("dentro")
        console.log(hash)
        if (this.state.logos[hash].isVisible) return;
    }

    shuffle(array) {
        let counter = array.length;

        // Mientras haya elementos en el array...
        while (counter > 0) {
            // Seleccionamos un índice aleatorio para un elemento
            let index = Math.floor(Math.random() * counter);

            // Reducimos contador
            counter--;

            // Intercambiamos el anterior elemento (random) con éste (counter)
            let temp = array[counter];
            array[counter] = array[index];
            array[index] = temp;
        }
        return array;
    }


    render() {
        //Recorremos los logos, guardamos en el elemento cada elemento.img
        /*
        let marcas = this.state.logos.map( 
            (marca) => <Marca logo={marca.img}/>
        );
        */

        let filas = [];
        let i = 0;

        //Recorremos las filas que auqramos
        for(let fila=0; fila<4; fila++){
            let columnas = [];
            //Dentro de las filas ponemos cada columna
            for(let columna=0; columna<5; columna++){
                let marca = this.state.logos[i].img;
                columnas.push(<Marca onClick={this.clicado} logo={marca}/>);
                i++;
            }
            filas.push(<div classname="fila">{columnas}</div>)
        }

        return (
            <div className="container">
                <Navbar color="dark">Memory</Navbar>
                {filas}
                <hr/>
                <Button color="primary" onClick={this.mezclar}>Mezclar</Button>
            </div>
        );
    }
}