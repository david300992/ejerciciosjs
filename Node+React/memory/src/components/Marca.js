import React, { Component } from 'react';
//CSS que afecta al componente
import './Marca.css';

//El que exporta por defecto, los demas tendran que empezar con export
export default class Marca extends Component {

    render() {
        return (
            <div className="celda">
                <img className="marca" src={'./imagenes/'+this.props.logo+'.jpg'}></img>
            </div>
        );
    }
}