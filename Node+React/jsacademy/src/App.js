import React, { Component } from 'react';
import './App.css';
//debe estar instalado: npm install react-router-dom
import { BrowserRouter, Route, NavLink } from "react-router-dom";

import {Container, Nav, NavItem, Button, Row, Col} from "reactstrap";

//deben estar definidos estos componentes, que generan el contenido para cada "página"
import Home from './components/HomeComponent'; 
import Cursos from './components/CursosComponent';
import Alumnos from './components/AlumnosComponent';
import NuevoAlumno from './components/NuevoAlumnoComponent';
import NuevoCurso from './components/NuevoCursoComponent';

const Espai = () => <div className='espai'></div>;

class App extends Component {
  constructor(props){
    super(props);

    //Cargamos datos por defecto
    let stadoInicial = this.cargaInicial();

    //Si estado inicial es null
    this.state = (stadoInicial) ? stadoInicial : {
      alumnos: [],
      cursos: [],
      lastIdCurso: 1,
      lastIdAlumno: 1
    }

    //Referenciamos al metodo
    this.nuevoAlumno = this.nuevoAlumno.bind(this);
    this.nuevoCurso = this.nuevoCurso.bind(this);
    //Metodos de borrado
    this.borraAlumno = this.borraAlumno.bind(this);
    this.borraCursos = this.borraCursos.bind(this);
    //Referencia al metodo para guardar y cargas los datos
    this.saveData = this.saveData.bind(this);
    this.loadData = this.loadData.bind(this);
    //Metodos de editar
    this.editaAlumno = this.editaAlumno.bind(this);
    this.editaCurso = this.editaCurso.bind(this);
  }

  //Recibe lo que enviar dentro del formulario
  nuevoAlumno(obj){
    //delete(ob.toAlumnos);
    //Añadimos un id al objeto
    obj.id= this.state.lastIdAlumno;

    //Uno el array actual con el alumno siguiente que me manda el objeto
    let nous = this.state.alumnos.concat([obj]);

    //Cambia el estado añadiendo alumnos
    this.setState({
      alumnos: nous,
      lastIdAlumno: this.state.lastIdAlumno+1
    })

  }

  //Recibe lo que enviar dentro del formulario
  nuevoCurso(obj){
    //delete(ob.toAlumnos);
    //Añadimos un id al objeto
    obj.id= this.state.lastIdCurso;

    //Uno el array actual con el alumno siguiente que me manda el objeto
    let nous = this.state.cursos.concat([obj]);

    //Cambia el estado añadiendo cursos
    this.setState({
      cursos: nous,
      lastIdCurso: this.state.lastIdCurso+1
    })

  }

  //carga de datos inicial
  cargaInicial(){
      var text = localStorage.getItem("academydata");
      if (text){
          return JSON.parse(text);
      }else{
        return null;
      }
  }

  //extra guardado de datos
  saveData(){
    var jsonData = JSON.stringify(this.state);
    localStorage.setItem("academydata", jsonData);
  }
            
  //carga de datos
  loadData(){
      var text = localStorage.getItem("academydata");
      if (text){
          var obj = JSON.parse(text);
          this.setState(obj);
      }
  }

  //Borra alumnos pasandole un id
  borraAlumno(id) {
    //Guardamos en variable todos los alumnos del state, quitandole el que tenga el id que le pasamos
    let nuevos = this.state.alumnos.filter((el)=>el.id!==id);
    //Actualizamos el state.alumnos con la variable anterior
    this.setState({
      alumnos: nuevos,
    })
  }

  //Borra alumnos pasandole un id
  borraCursos(id) {
    console.log(id)
    //Guardamos en variable todos los alumnos del state, quitandole el que tenga el id que le pasamos
    let nuevos = this.state.cursos.filter((el)=>el.id!==id);
    //Actualizamos el state.cursos con la variable anterior
    this.setState({
      cursos: nuevos,
    })
  }

  //Editar alumno
  editaAlumno(ob) {
    
    //Guardamos todos los alumnos menos el que tiene el id del objeto que le pasamos
    let nous = this.state.alumnos.filter((item)=>item.id!==ob.id);
    //Añadimos el ob que le pasamos (alumno modificado) y concatenamos a nous
    nous = nous.concat([ob]);
    //Ordenamos, le pasamos la funcion compararId, con la que se ordena
    nous.sort(this.compararId);
    //Actualizamos state
    this.setState({
      alumnos: nous
    })
  }

  //Editar alumno
  editaCurso(ob) {
    //Guardamos todos los alumnos menos el que tiene el id del objeto que le pasamos
    let nous = this.state.cursos.filter((item)=>item.id!==ob.id);
    //Añadimos el ob que le pasamos (curso modificado) y concatenamos a nous
    nous = nous.concat([ob]);
    //Ordenamos, le pasamos la funcion compararId, con la que se ordena
    nous.sort(this.compararId);
    //Actualizamos state
    this.setState({
      cursos: nous
    })
  }

  //Compara las id
  compararId(a, b){
    if(a.id < b.id){
      return -1;
    }else if(a.id > b.id){
      return 1;
    }else{
      return 0;
    }
  }

  render() {
    return (
      <BrowserRouter>
        <Container>
        <h1>JS-Academy</h1>
          <Espai />
          <Nav>
            <NavItem></NavItem>
            <NavItem>
              <NavLink  exact className='link' to='/'>HOME</NavLink>
            </NavItem>
            <NavItem>
              <NavLink exact className='link' to='/cursos'>CURSOS</NavLink>
            </NavItem>
            <NavItem>
              <NavLink exact className='link' to='/alumnos' >ALUMNOS</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className='link' to='/alumnos/crea' >NUEVO ALUMNO</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className='link' to='/cursos/crea' >NUEVO CURSO</NavLink>
            </NavItem>
          </Nav>
          
          <Espai />

          <Route exact path="/" component={Home} />
          {/*
           en render le pasamos el elemento Cursos que le pasamos el state cursos
          */}
          <Route exact path="/cursos" render={()=><Cursos cursos={this.state.cursos} onBorrar={this.borraCursos} />} />
          {/* Pasamos alumnos y la funcion de borrar */}
          <Route exact path="/alumnos" render={()=><Alumnos alumnos={this.state.alumnos} onBorrar={this.borraAlumno}/>} />
          <Route exact path="/alumnos/crea" render={()=><NuevoAlumno onEnviar={this.nuevoAlumno} cursos={this.state.cursos} />} />
          <Route exact path="/cursos/crea" render={()=><NuevoCurso onEnviar={this.nuevoCurso}/>} />
          {/* Nueva Route para editar alumno, le pasamos las props al render y los alumnos */}
          <Route path="/alumnos/crea/:alumnoId" render={(props)=><NuevoAlumno onEnviar={this.editaAlumno} alumnos={this.state.alumnos} cursos={this.state.cursos} {...props}/>} />
          {/* Nueva Route para editar curso, le pasamos las props al render y los alumnos */}
          <Route path="/cursos/crea/:cursoId" render={(props)=><NuevoCurso onEnviar={this.editaCurso} cursos={this.state.cursos} {...props}/>} />
          <br /><br />
          <Row>
            <Col>
            <Button color="success" className="btn-margin btn-sm" onClick={this.saveData}>Guardar</Button>   
            <Button color="warning" className="btn-margin btn-sm" onClick={this.loadData}>Recuperar</Button>   
            </Col>
          </Row>
        </Container>
      </BrowserRouter>
    );
  }
}

export default App;