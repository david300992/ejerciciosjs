import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class NuevoCurso extends Component {
  constructor(props) {
    super(props);
    //Estado por defecto
    let estado = {
      nombre: '',
      especialidad: ''
    }

    //Miramos si existen datos, primero los parametros y luego cursos
    if(this.props.match && this.props.cursos){
      //Guarda id que pasamos
      let idCursoEditar = parseInt(this.props.match.params.cursoId);
      //Filtro para obtener datos del alumno a editar
      estado = this.props.cursos.filter((item)=>(item.id===idCursoEditar))[0];
    }

    //irCursos para redirigir a listado de cursos una vez enviados los datos del form
    estado.irCursos=false;

    //Asignamos al state nuestro estado por defecto
    this.state = estado;

    //Propiedades por cada valor del formulalrio
    /*
    this.state = {
      nombre: '',
      especialidad: '',
      irCursos: false
    };
    */

    //Referenciamos al metodo
    this.mirarCambioInput = this.mirarCambioInput.bind(this);
    this.enviar = this.enviar.bind(this);
  }

  //Metodo que notara los cambios de cualquier input
  mirarCambioInput(event) {
    //En target guardamos toda la etiqueta input
    const target = event.target;
    //Aqui se guarda el tipo de input, si es checkbox mira el valor
    const value = target.type === 'checkbox' ? target.checked : target.value;
    //Coge el valor name del input
    const name = target.name;

    //Como hemos puesto el valor name igual que el nombre de las propiedades a cambiar las modificamos pasandole las constantes
    this.setState({
      [name]: value
    });
  }

  //Redigir el contenido de State a props "onChange" que nos hayan facilitado
  enviar(evt) {
    //Envia el this.state, al metodo onEnviar pasandolo al App.js
    this.props.onEnviar(this.state);
    //Evita que el formulario se comporte como por defecto
    evt.preventDefault();
    //Cambio la propiedad state irCursos a true para mirar de cambiar la pagina
    this.setState({irCursos: true});
  }

  render() {
    //Si es true cambio a la lista de cursos
    if(this.state.irCursos === true){
      return <Redirect to='/cursos' />
    }

    return (
      <Form onSubmit={this.enviar}>
        <FormGroup>
          <Label for="exampleNombre">Nombre</Label>
          {/* Vinculamos input con la propiedad */}
          <Input type="text" name="nombre" id="exampleNombre" placeholder="Nombre del curso" value={this.state.nombre}  onChange={this.mirarCambioInput} />
        </FormGroup>
        <FormGroup>
          <Label for="exampleEspecialidad">Especialidad</Label>
          {/* Vinculamos input con la propiedad */}
          <Input type="text" name="especialidad" id="exampleEspecialidad" placeholder="Especialidad" value={this.state.especialidad}  onChange={this.mirarCambioInput} />
        </FormGroup>
        <Button color="primary">Enviar</Button>
      </Form>
    );
  }
}

export default NuevoCurso;