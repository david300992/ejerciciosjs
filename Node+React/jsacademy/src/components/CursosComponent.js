import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Table, Button } from 'reactstrap';

class Cursos extends Component {
  constructor(props) {
    super(props);

    //Estados para ver si vamos a nuevo alumno y el id que modificaremos
    this.state = {
      iraNuevoCurso: false,
      modificarId: null
    }

    //Referencias a las funciones
    this.iraCreaCurso = this.iraCreaCurso.bind(this);
    this.modificar = this.modificar.bind(this);
  }

  //Indicador para pasar a la vista de crear curso
  iraCreaCurso(){
    this.setState({
      iraNuevoCurso : true
    })
  }

  //Id que modificar
  modificar(cursoId){
    this.setState({
      modificarId: cursoId
    })
  }

  render() {
    //Si se pulsa el boton "nuevo curso"
    if (this.state.iraNuevoCurso === true) {
      //Redirigimos a la pagina alumno/crea
      return <Redirect to='/cursos/crea'  />
    }
    //Si se pulsa "editar curso", el state.modificarId existe
    if(this.state.modificarId){
      return <Redirect  to={'/cursos/crea/'+this.state.modificarId} />
    }
    
    //Cursos que has creado en array
    let cursos = this.props.cursos

    //Donde se metera cada curso
    let cursostr = [];
    
    //Guardas el this
    let that = this;
    
    //Recorremos cursos
    cursos.forEach( function(curso, indice, array) {
      cursostr.push(
        <tr key={curso.id}>
          <th scope="row">{curso.id}</th>
          <td>{curso.nombre}</td>
          <td>{curso.especialidad}</td>
          <td><i className='fa fa-lg fa-trash borrar' onClick={()=>that.props.onBorrar(curso.id)}></i></td>
          <td><i className='fa fa-lg fa-edit modificar' onClick={()=>that.modificar(curso.id)}></i></td>
        </tr>)
    });
    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Especialidad</th>
            </tr>
          </thead>
          <tbody>
            {cursostr}
          </tbody>
        </Table>
        <br />
        <Button color="danger" onClick={this.iraCreaCurso}>Nuevo</Button>
      </div>
    );
  }
}

export default Cursos;
