import React, { Component } from 'react';
import './Memory.css';
import {Container, Row, Col} from 'reactstrap';

import {LOGOS} from '../data/logos';

import Item from './Item.js';

class Memory extends Component {

  constructor(props){
      super(props);

      this.state = {
          logos: this.shuffleLogos(),
          winner: false,
          parelles: 0, 
          check1: null,  
          check2: null,  
        }
      
      this.clico = this.clico.bind(this);
      this.init = this.init.bind(this);
  }



  clico(item){
    // item.isVisible=true;
    // this.forceUpdate();
    let parelles = this.state.parelles;

    if (this.state.winner) return;

    if (this.state.logos[item.index].isVisible){
      if (parelles === 9) {
        //winner!
        console.log("WINNER!");
        this.setState({winner: true});
      } else {
         return;
      }
    }

    let xlogos = JSON.parse(JSON.stringify(this.state.logos));
    xlogos[item.index].isVisible=true;
    
    if (this.state.check1) {
      if (this.state.check2) {
        if (this.state.check2.img === this.state.check1.img) {
          parelles++;
          console.log("parella!");
        } else {
          xlogos[this.state.check1.index].isVisible=false;
          xlogos[this.state.check2.index].isVisible=false;
        }
        // console.log("check1 i 2 borrats, nou check1:" + xlogos[item.index].img );
        this.setState({
          check1: xlogos[item.index],
          check2: null,
          parelles,
          logos: xlogos
        });
      
      } else {
        // console.log("check2:" + xlogos[item.index].img);
        this.setState({
          check2: xlogos[item.index],
          logos: xlogos
        });
      }

    } else {
      // console.log("check1:" + xlogos[item.index].img);
      this.setState({
        check1: xlogos[item.index],
        logos: xlogos
      });
      
    }



  }

  
  init(){
    this.setState({
      logos: this.shuffleLogos(),
      winner: false,
      check1: null,  
      check2: null,  
      intents: 0
    })
  }

shuffleLogos(){
  let idxs = this.shuffle([0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9]);
  let barrejats = [];
  let i =0;
  idxs.forEach(id => {
    barrejats.push({img: LOGOS[id], isVisible: false, index: i });
    i++;
  });
  return barrejats;
}

shuffle(array) {
    let counter = array.length;

    // Mientras haya elementos en el array...
    while (counter > 0) {
        // Seleccionamos un índice aleatorio para un elemento
        let index = Math.floor(Math.random() * counter);

        // Reducimos contador
        counter--;

        // Intercambiamos el anterior elemento (random) con éste (counter)
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}


  render() {

    let cells=[];
    let rows = [];
    let index = 0;

    for (let fila = 0; fila < 4; fila++) {
      cells=[];
      for (let columna = 0; columna < 5; columna++) {
        cells.push(<Item key={index} index={index}  onClick={this.clico} item={this.state.logos[index]}  />);
        index++;
      }
      rows.push(<div className='cssRow' key={'fila'+fila}>{cells}</div>)
    }


    return (
      <Container className='cssTable'>
        <Row className='cssRow'>
          <Col xs='8'>
           
            <div className='cssTable'>
              {rows}
            </div>
            <hr />
          <button className="btn btn-primary" onClick={this.init} >Init</button>
          </Col>
        </Row>
      </Container>
     
    );
  }
}

export default Memory;
