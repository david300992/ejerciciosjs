import React from 'react';

import './header.css';



export default class Header extends React.Component {

    render() {
        return (
                <div>
                    <nav className="navbar navbar-dark bg-dark">
                        <span className="navbar-text">{this.props.titol}</span>
                    </nav>

                    <div className="espai50"></div>
                </div>
        );
    }

}