import React, { Component } from 'react';
import './App.css';
import Memory from './components/Memory';
import Header from './components/Header';

class App extends Component {
  render() {
    return (
      <div>
        <Header titol="Memory" />
        <Memory />
      </div>
    );
  }
}

export default App;
