import React, { Component } from 'react';

export default class Item extends Component {


    render(){
        let imatge = (this.props.item.isVisible) ? '/img/'+this.props.item.img : '/img/quisap.jpg';
        return(
            <div className='cssCell'>
            <img className='logo' onClick={()=>this.props.onClick(this.props.item)} src={imatge} alt='' />
            </div>
        );
        
    }

}
