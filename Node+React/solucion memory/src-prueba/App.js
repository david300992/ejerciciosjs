import React, { Component } from 'react';
import './App.css';

import Header, {Footer} from './components/Header.js';

import Memory from './components/Memory.js';


class App extends Component {
  render() {
    return (
      <div>
        <Header titulo="Memory" />
        <Memory />
      </div>
    );
  }
}

export default App;
