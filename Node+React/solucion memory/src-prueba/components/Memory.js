import React, { Component } from 'react';
import './memory.css';

import Marca from './Marca.js';


export default class Memory extends Component {

  constructor(props){
    super(props);
    const LOGOS = ["cocacola", "evian", "pringles", 
    "mms", "kitkat", "lipton", "oldelpaso",
    "pepsi", "milka", "maggi",
    "cocacola", "evian", "pringles", 
    "mms", "kitkat", "lipton", "oldelpaso",
    "pepsi", "milka", "maggi",
  ]


    this.state = {
      logos: LOGOS.map((logo) => {return {nombre: logo, isVisible: true}; } )
    }

    this.mezclar = this.mezclar.bind(this);
    this.shuffle = this.shuffle.bind(this);
  }

  mezclar(){
      this.setState({
        logos: this.shuffle(this.state.logos)
      })
  }

  shuffle(array) {
    let counter = array.length;

    // Mientras haya elementos en el array...
    while (counter > 0) {
        // Seleccionamos un índice aleatorio para un elemento
        let index = Math.floor(Math.random() * counter);

        // Reducimos contador
        counter--;

        // Intercambiamos el anterior elemento (random) con éste (counter)
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}


  render() {
    
    let filas = [];
    let i=0;

    for (let fila=0; fila < 4; fila++){
      let columnas = [];
      for (let columna=0; columna < 5; columna++){
        let marca = this.state.logos[i];
        columnas.push(<Marca logo={marca} />);
        i++;
      }
      filas.push(<div className="fila">{columnas}</div>)
    }

    return (
      <div className="tabla">
        {filas}
        <hr />
        <button onClick={this.mezclar} >Mezclar</button>
      </div>
    );
  }
}

