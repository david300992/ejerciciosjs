import React, { Component } from 'react';
import './marca.css';


export default class Marca extends Component {
  render() {

    let imagenAmostrar = (this.props.logo.isVisible) ? '/img/'+this.props.logo.nombre+".jpg" : '/img/quisap.jpg';

    return (
      <div className="celda">
        <img className="marca" src={imagenAmostrar} alt=''/>
      </div>
    );
  }
}
