import React, { Component } from 'react';
import './Header.css';


//Extiende de Component=React.Component
export class Footer extends Component {
    render() {
        return (
        <div className="container">
            <h1>The end</h1>
            <hr/>
        </div>
        );
    }
}

//Extiende de Component=React.Component
export default class Header extends Component {
  render() {
    return (
      <div className="container">
          <h1 className="especial">Hola!</h1>
          <hr/>
      </div>
    );
  }
}
