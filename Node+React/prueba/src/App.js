import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

//Importamos el componente Header que tiene una clase Header y otra Footer
import Header, {Footer} from './components/Header.js'
import {DIAS_SEMANA} from './components/Dist.js'

//Importar elementos del reactstrap
import { Button } from 'reactstrap';

class App extends Component {
  render() {
    return (
      <div className="container">
          <Header />
          <h2>{DIAS_SEMANA[0]}</h2>
          <img src="/img/descarga.png" />
          <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in sapien magna. Ut vel tempor enim, non malesuada enim. Morbi dapibus rhoncus lacus sit amet imperdiet. Nunc eget vulputate eros. Etiam maximus finibus ligula ut tempor. Nam nec mi non ex euismod lacinia. Pellentesque auctor nisi at erat venenatis, id egestas velit tincidunt. Aliquam nec tellus turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
          </p>
          <Footer />
      </div>
    );
  }
}

export default App;