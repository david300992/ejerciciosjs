
const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017/agenda'
const dbname = 'agenda';

//Hacemos la conexion, url de arriba, el userNewUrl lo necesita
//client es la conexion/enlace a la bbdd
MongoClient.connect(url, {useNewUrlParser: true}, (err, client)=>{
    //Abrimos conexion con la bbdd
    const db = client.db(dbname);
    //Dentro de la conexion usamos la coleccion contactos
    const collection = db.collection("contactos");

    //Busca lo que encuentra y lo concierte en array, llamado docs
    collection.find({}).toArray((err, docs)=>{
        docs.forEach((item)=> console.log(item.nombre));
        console.log(docs);
    })
});