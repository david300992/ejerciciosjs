//Una milla cuantos km son
const unaMilla = 1.60934;
//Un grado celsius cuantos Fahrenheit son
const unCelsius = 33.8;

//Recibe millas y devuelve km
function m2km (m){
    var km;
    km= m*unaMilla;
    return km;
}
/*
1milla = '1,60934'
       ='5km'
*/
//Recibe km y devuelve millas
function km2m (km){
    var millas;
    millas = km/unaMilla
    return millas;
}
/*
1celsius = '33.8 fah'
       ='100 fah'
*/
//Recibe grados Celsius y devuelve Fahrenheit
function c2f (c){
    var fahren;
    fahren= c*unCelsius;
    return fahren;
}
//Recibe grados Fahrenheit y devuelve Celsius
function f2c (f){
    var celsius;
    //f = 33,8
    celsius = f/unCelsius
    return celsius;
}

exports.makm = m2km;
exports.kmam = km2m;
exports.caf = c2f;
exports.fac = f2c;