console.log("hola mundo");

const rectangulo = require('./rectangulo.js');

const conversiones = require('./conversiones.js');

const a = 3;
const b = 5;
//unos 8,04672km
const millas = 5;
//unas 3,10686 millas
const km = 5;
//unos 75,2 fah
const gcel = 24;
//unos 26,6667 cel
const gfah = 80;
//console.log("El area de un rectangulo de lados "+ a+ " y "+b+" es "+ rectangulo.area(a,b));
//console.log("El perimetro de un rectangulo de lados "+ a+ " y "+b+" es "+ rectangulo.perimetro(a,b));
//console.log(millas + " millas son " + conversiones.makm(millas) + " km ");
//console.log(km + " km son " + conversiones.kmam(km) + " millas ");
//console.log(gcel + " grados Celsius son " + conversiones.caf(gcel) + " grados Fahrenheit ");
//console.log(gfah + " grados Fahrenheit son " + conversiones.fac(gfah) + " grados Celsius ");

//Ejemplo servidor web
/*
//importamos el modulo http
const http = require('http');

//Constantes del servidor
const hostname = 'localhost';
const port = 3000;

//req es lo que nos llega y res escribimos datos
const server = http.createServer((req, res) => {
    console.log("Ha llegado una peticion")
    //console.log(req.headers);
    res.statusCode = 200;
    //Que tipo de respuesta sera, en este caso html
    res.setHeader('Content-Type', 'text/html');
    //Contenido de la pagina que envio
    res.end('<html><head></head><body><h1>Hola caracola!</h1></body></html > ');
})
server.listen(port, hostname, () => {
    console.log(`Escuchando por http://${hostname}:${port}/`);
});
*/

//Servidor con Express
const express = require('express')
const app = express()
app.get('/', function (req, res) {
    res.send('Hola Express!')
})

//Codifica datos del post
app.use(express.urlencoded());
/*
app.get('/hola', function (req, res) {
    res.send('<form action="/hola" method="post">Tu nombre:<input type="text" name="nombre"><input type="submit" value="Enviar"></form>')
})
app.post('/hola', function (req, res) {
    var name = req.body.nombre;
    res.send(`<h1> Hola ${name}</h1>`);
})
app.post('/adios', function (req, res) {
    var name = req.body.nombre;
    res.send(`<h1> Adios ${name}</h1>`);
})
*/

app.use(express.static(__dirname+'/public'));

//Pagina que convierta millas en km
/*
app.get('/m2km', function (req, res) {
    res.send('<form action="/m2km" method="post">Millas:<input type="text" name="millas"><input type="submit" value="Enviar"></form>')
})
*/
//

app.post('/m2km', function (req, res) {
    var millas = req.body.millas;
    var km = conversiones.makm(millas)
    res.send(`<h1> ${millas} millas son ${km} Km</h1>`);
})
//Pagina que concierta Fahrenheit en Celsius
app.get('/f2c', function (req, res) {
    res.send('<form action="/f2c" method="post">Grados Fahrenheit:<input type="text" name="fahrenheit"><input type="submit" value="Enviar"></form>')
})
app.post('/f2c', function (req, res) {
    var fahrenheit = req.body.fahrenheit;
    var celsius = conversiones.fac(fahrenheit*1);
    console.log(fahrenheit, celsius);
    res.send(`<h1> ${fahrenheit}º Fahrenheit son  ${celsius}º Celsius</h1>`);
})
app.listen(3000, function () {
    console.log('Escuchando en puerto 3000!')
})