var canvas = document.getElementById("paper");
var ctx = canvas.getContext("2d");  

var alto = canvas.height;
var ancho = canvas.width;

//https://developer.mozilla.org/es/docs/Games/Workflows/Famoso_juego_2D_usando_JavaScript_puro

var obstaculos=[];
obstaculos.push(new  Rectangulo(ctx,123, 52, 50, 50, "red"));
obstaculos.push(new  Rectangulo(ctx,50, 50, 20, 100, "red"));


redondas=[];

redondas.push(new Bola(20, 20, 10, "red"));
redondas.push(new Bola(20, 10, 10, "red"));
redondas.push(new Bola(100, 20, 1, "blue"));
redondas.push(new Bola(15, 200, 1, "green"));

//redondas.push(new Bola(50, 100, 12, "green"));

function refresca(){
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  
  obstaculos.forEach(function(r){
    r.pinta();
  });

  
  redondas.forEach(function(r){
    r.pinta();
  });
}

setInterval(refresca, 10);


//setInterval(actualiza, 10);

function Bola(x0,y0,radi,color){
  this.color = color;
  this.radi = radi;
  this.x = x0;
  this.y = y0;
  this.dx = 1;
  this.dy = 1;
  
  this.detectaLimites = function(){
    var iob = {x: 0, y: 0, w: ancho, h: alto};
    //console.log(this.y,iob.y+iob.h-this.radi);
    
    if(this.y > iob.y+iob.h-this.radi || this.y < iob.y+this.radi ) {
      this.dy = -this.dy;
    }
    
    if(this.x > iob.x+iob.w-this.radi || this.x < iob.x+this.radi ) {
      this.dx = -this.dx;
    }
  };
  
  this.detectaColisiones = function(){
    var that=this;
    obstaculos.forEach(function(ob){
      
      var sobre = ob.y;
      var sota = ob.y+ob.dy;
      var esquerra = ob.x;
      var dreta = ob.x+ob.dx;
      
      var novax = that.x+that.dx;
      var novay = that.y+that.dy;
      
      
      if (novay >= sobre-that.radi && that.y<sobre-that.radi ) {
        if (novax>=esquerra-that.radi && novax<dreta+that.radi){
          that.dy = -that.dy;
          return;
        }
      }  
      
      if (novay <= sota+that.radi && that.y>sota+that.radi ) {
        if (novax>=esquerra-that.radi && novax<dreta+that.radi){
          that.dy = -that.dy;
          return;
        }
      } 
      
      //console.log (`${novax}, ${novay} -- ${dreta} ${sobre}`);
    
      if (novax <= dreta+that.radi && that.x>dreta+that.radi ) {
        if (novay>=sobre && novay<sota){
          that.dx = -that.dx;
          return;
        }
      } 
    
      
      if (novax>=esquerra-that.radi && that.x<esquerra-that.radi ) {
        if (novay>=sobre && novay<sota){
          that.dx = -that.dx;
          return;
        }
      } 
     
    
    
    
    });
  }

    this.pintaF = function() {
  
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radi, 0, Math.PI*2);
    ctx.fillStyle = this.color;
    ctx.fill();
    ctx.closePath();
 
    }
    
  this.pinta = function() {
  
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radi, 0, Math.PI*2);
    ctx.strokeStyle = this.color;
    ctx.stroke();
    ctx.closePath();
    
    this.detectaColisiones();
    this.detectaLimites();
    
    
    this.x = this.x+this.dx;
    this.y = this.y+this.dy;
    
 
    }
}
    


function Rectangulo(ctx0, x0,y0,ancho,alto,color){
    this.x = x0;
    this.y = y0;
    this.dx = ancho;
    this.dy = alto;
    this.color = color;
    this.contexto = ctx0;
  
    this.pinta = function(){
      this.contexto.beginPath();
      this.contexto.strokeStyle = this.color;
      this.contexto.rect(this.x,this.y,this.dx,this.dy);
      this.contexto.stroke();
      this.contexto.closePath();
    }
    
     this.pintaF = function(){
      this.contexto.beginPath();
      this.contexto.fillStyle = this.color;
      this.contexto.rect(this.x,this.y,this.dx,this.dy);
      this.contexto.fill();
      this.contexto.closePath();
    }
     
     this.insidex = function(x,rad){
       return (x>this.x && x<this.x+this.dx) ? true : false;
     }
     
   this.insidey = function(y,rad){
       return (y>this.y && y<this.y+this.dy) ? true : false;
     }
     
}

