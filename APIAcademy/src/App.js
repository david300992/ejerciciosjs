import React, { Component } from 'react';
import './App.css';
//debe estar instalado: npm install react-router-dom
import { BrowserRouter, Route, NavLink } from "react-router-dom";

import {Container, Nav, NavItem} from "reactstrap";

import API from './components/Api';

//deben estar definidos estos componentes, que generan el contenido para cada "página"
import Home from './components/HomeComponent'; 
import Cursos from './components/CursosComponent';
import Alumnos from './components/AlumnosComponent';
import NuevoAlumno from './components/NuevoAlumnoComponent';
import NuevoCurso from './components/NuevoCursoComponent';
//Añadido
import Profesores from './components/ProfesoresComponent';
import Ordenadores from './components/OrdenadoresComponent';
import NuevoOrdenador from './components/NuevoOrdenadorComponent';
import NuevoProfesor from './components/NuevoProfesorComponent';
//Ver info del curso
import InfoCurso from './components/InfoCursoComponent';

const Espai = () => <div className='espai'></div>;

class App extends Component {
  constructor(props){
    super(props);

    //Cargamos datos por defecto
    let stadoInicial = this.cargaInicial();

    //Si estado inicial es null
    this.state = (stadoInicial) ? stadoInicial : {
      alumnos: [],
      cursos: [],
      profesores: [],
      ordenadores: [],
      lastIdCurso: 1,
      lastIdAlumno: 1
    }

    //Referenciamos al metodo
    this.nuevoAlumno = this.nuevoAlumno.bind(this);
    this.nuevoCurso = this.nuevoCurso.bind(this);
    this.nuevoProfesor = this.nuevoProfesor.bind(this);
    this.nuevoOrdenador = this.nuevoOrdenador.bind(this);
    //Metodos de borrado
    this.borraAlumno = this.borraAlumno.bind(this);
    this.borraCursos = this.borraCursos.bind(this);
    this.borraProfesor = this.borraProfesor.bind(this);
    this.borraOrdenador = this.borraOrdenador.bind(this);
    
    //Metodos de editar
    this.editaAlumno = this.editaAlumno.bind(this);
    this.editaCurso = this.editaCurso.bind(this);
    this.editaProfesor = this.editaProfesor.bind(this);
    this.editaOrdenador = this.editaOrdenador.bind(this);

    this.cargaInicial = this.cargaInicial.bind(this);
  }

  //Recibe lo que enviar dentro del formulario
  nuevoAlumno(obj){
    //Uno el array actual con el alumno siguiente que me manda el objeto
    let nous = this.state.alumnos.concat([obj]);

    //Cambia el estado añadiendo alumnos
    this.setState({
      alumnos: nous,
      lastIdAlumno: this.state.lastIdAlumno+1
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.nuevoAlumno(obj);
    this.cargaInicial();

  }

  //Recibe lo que enviar dentro del formulario
  nuevoCurso(obj){
    //delete(ob.toAlumnos);
    //Añadimos un id al objeto
    //obj._id= this.state.lastIdCurso;

    //Uno el array actual con el alumno siguiente que me manda el objeto
    let nous = this.state.cursos.concat([obj]);

    //Cambia el estado añadiendo cursos
    this.setState({
      cursos: nous,
      lastIdCurso: this.state.lastIdCurso+1
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.nuevoCurso(obj);
    this.cargaInicial();

  }
  //Recibe obj que envia dentro del formulario
  nuevoProfesor(obj){
    //Uno el array actual con el alumno siguiente que me manda el objeto
    let nous = this.state.profesores.concat([obj]);

    //Cambia el estado añadiendo cursos
    this.setState({
      profesores: nous,
      lastIdProfesor: this.state.lastIdProfesor+1
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.nuevoProfesor(obj);
    this.cargaInicial();
  }
  //Recibe obj que envia dentro del formulario
  nuevoOrdenador(obj){
    //delete(ob.toAlumnos);
    //Añadimos un id al objeto
    //obj._id= this.state.lastIdCurso;

    //Uno el array actual con el alumno siguiente que me manda el objeto
    let nous = this.state.ordenadores.concat([obj]);

    //Cambia el estado añadiendo cursos
    this.setState({
      ordenadores: nous,
      lastIdCurso: this.state.lastIdCurso+1
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.nuevoOrdenador(obj);
    this.cargaInicial();
  }

  //carga de datos inicial
  cargaInicial(){
    //LLamamos a la funcion de carga de la BBDD si hay resultados carga el state
    API.getInicial((result)=> {
      if(result) {
        this.setState(result);
      }
    });
  }

  //Borra alumnos pasandole un id
  borraAlumno(_id) {
    //Guardamos en variable todos los alumnos del state, quitandole el que tenga el id que le pasamos
    let nuevos = this.state.alumnos.filter((el)=>el._id!==_id);
    //Actualizamos el state.alumnos con la variable anterior
    this.setState({
      alumnos: nuevos,
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.borraAlumno(_id);
  }

  //Borra alumnos pasandole un id
  borraCursos(_id) {
    
    //Guardamos en variable todos los alumnos del state, quitandole el que tenga el id que le pasamos
    let nuevos = this.state.cursos.filter((el)=>el._id!==_id);
    //Actualizamos el state.cursos con la variable anterior
    this.setState({
      cursos: nuevos,
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.borraCurso(_id);
  }

  //Borra profesores pasandole un id
  borraProfesor(_id){
    //Guardamos en variable todos los alumnos del state, quitandole el que tenga el id que le pasamos
    let nuevos = this.state.profesores.filter((el)=>el._id!==_id);
    //Actualizamos el state.cursos con la variable anterior
    this.setState({
      profesores: nuevos,
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.borraProfesor(_id);
  }

  //Borra ordenadores pasandole un id
  borraOrdenador(_id){
    //Guardamos en variable todos los alumnos del state, quitandole el que tenga el id que le pasamos
    let nuevos = this.state.ordenadores.filter((el)=>el._id!==_id);
    //Actualizamos el state.ordenadores con la variable anterior
    this.setState({
      ordenadores: nuevos,
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.borraOrdenador(_id);
  }

  //Editar alumno
  editaAlumno(ob) {
    //Guardamos todos los alumnos menos el que tiene el _id del objeto que le pasamos
    let nous = this.state.alumnos.filter((item)=>item._id!==ob._id);
    //Añadimos el ob que le pasamos (alumno modificado) y concatenamos a nous
    nous = nous.concat([ob]);
    //Ordenamos, le pasamos la funcion compararId, con la que se ordena
    nous.sort(this.compararId);
    //Actualizamos state
    this.setState({
      alumnos: nous
    })
    
    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.editaAlumno(ob, this.cargaInicial);
  }

  //Editar alumno
  editaCurso(ob) {
    //Guardamos todos los alumnos menos el que tiene el id del objeto que le pasamos
    let nous = this.state.cursos.filter((item)=>item._id!==ob._id);
    //Añadimos el ob que le pasamos (curso modificado) y concatenamos a nous
    nous = nous.concat([ob]);
    //Ordenamos, le pasamos la funcion compararId, con la que se ordena
    nous.sort(this.compararId);
    //Actualizamos state
    this.setState({
      cursos: nous
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.editaCurso(ob, this.cargaInicial);
  }

  //Edita profesor
  editaProfesor(ob){
    //Guardamos todos los alumnos menos el que tiene el id del objeto que le pasamos
    let nous = this.state.profesores.filter((item)=>item._id!==ob._id);
    //Añadimos el ob que le pasamos (curso modificado) y concatenamos a nous
    nous = nous.concat([ob]);
    //Ordenamos, le pasamos la funcion compararId, con la que se ordena
    nous.sort(this.compararId);
    //Actualizamos state
    this.setState({
      profesores: nous
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.editaProfesor(ob, this.cargaInicial);
  }

  //Edita ordenador
  editaOrdenador(ob){
    //Guardamos todos los alumnos menos el que tiene el id del objeto que le pasamos
    let nous = this.state.ordenadores.filter((item)=>item._id!==ob._id);
    //Añadimos el ob que le pasamos (curso modificado) y concatenamos a nous
    nous = nous.concat([ob]);
    //Ordenamos, le pasamos la funcion compararId, con la que se ordena
    nous.sort(this.compararId);
    //Actualizamos state
    this.setState({
      ordenadores: nous
    })

    //Va al archivo API y usa nuevoAlumno pasandole el objeto alumno
    API.editaOrdenador(ob, this.cargaInicial);
  }

  //Compara las id
  compararId(a, b){
    if(a._id < b._id){
      return -1;
    }else if(a._id > b._id){
      return 1;
    }else{
      return 0;
    }
  }

  render() {
    return (
      <BrowserRouter>
        <Container>
        <h1>JS-Academy</h1>
          <Espai />
          <Nav>
            <NavItem></NavItem>
            <NavItem>
              <NavLink  exact className='link' to='/'>HOME</NavLink>
            </NavItem>
            <NavItem>
              <NavLink exact className='link' to='/cursos'>CURSOS</NavLink>
            </NavItem>
            <NavItem>
              <NavLink exact className='link' to='/alumnos' >ALUMNOS</NavLink>
            </NavItem>
            <NavItem>
              <NavLink exact className='link' to='/profesores' >PROFESORES</NavLink>
            </NavItem>
            <NavItem>
              <NavLink exact className='link' to='/ordenadores' >ORDENADORES</NavLink>
            </NavItem>
          </Nav>
          
          <Espai />

          <Route exact path="/" component={Home} />
          {/*
           en render le pasamos el elemento Cursos que le pasamos el state cursos
          */}
          <Route exact path="/cursos" render={()=><Cursos cursos={this.state.cursos} onBorrar={this.borraCursos} />} />
          <Route exact path="/cursos/crea" render={()=><NuevoCurso onEnviar={this.nuevoCurso}/>} />
          {/* Nueva Route para editar curso, le pasamos las props al render y los alumnos */}
          <Route exact path="/cursos/crea/:cursoId" render={(props)=><NuevoCurso onEnviar={this.editaCurso} cursos={this.state.cursos} {...props}/>} />
          {/* Informacion del curso */}
          <Route exact path="/curso/:cursoId" render={(props)=><InfoCurso infoCurso={this.state.infoCurso} {...props}/>}/>


          {/* Pasamos alumnos y la funcion de borrar */}
          <Route exact path="/alumnos" render={()=><Alumnos alumnos={this.state.alumnos} onBorrar={this.borraAlumno}/>} />
          <Route exact path="/alumnos/crea" render={()=><NuevoAlumno onEnviar={this.nuevoAlumno} cursos={this.state.cursos} />} />
          {/* Nueva Route para editar alumno, le pasamos las props al render y los alumnos */}
          <Route exact path="/alumnos/crea/:alumnoId" render={(props)=><NuevoAlumno onEnviar={this.editaAlumno} alumnos={this.state.alumnos} cursos={this.state.cursos} {...props}/>} />
      
          <Route exact path="/profesores" render={()=><Profesores profesores={this.state.profesores} onBorrar={this.borraProfesor} />} />
          <Route exact path="/profesores/crea" render={()=><NuevoProfesor onEnviar={this.nuevoProfesor} profesores={this.state.profesores} cursos={this.state.cursos}/>} />
          <Route exact path="/profesores/crea/:profesorId" render={(props)=><NuevoProfesor onEnviar={this.editaProfesor} profesores={this.state.profesores} cursos={this.state.cursos} {...props}/>} />
          

          <Route exact path="/ordenadores" render={()=><Ordenadores ordenadores={this.state.ordenadores} onBorrar={this.borraOrdenador} />} />
          <Route exact path="/ordenadores/crea" render={()=><NuevoOrdenador onEnviar={this.nuevoOrdenador} alumnos={this.state.alumnos} />} />
          <Route exact path="/ordenadores/crea/:ordenadorId" render={(props)=><NuevoOrdenador onEnviar={this.editaOrdenador} ordenadores={this.state.ordenadores} alumnos={this.state.alumnos} {...props}/>} />
          


          
        </Container>
      </BrowserRouter>
    );
  }
}

export default App;