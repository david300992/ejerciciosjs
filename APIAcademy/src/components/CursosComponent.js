import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Table, Button } from 'reactstrap';

class Cursos extends Component {
  constructor(props) {
    super(props);

    //Estados para ver si vamos a nuevo alumno y el id que modificaremos
    this.state = {
      iraNuevoCurso: false,
      modificarId: null,
      verInfoId: null
    }

    //Referencias a las funciones
    this.iraCreaCurso = this.iraCreaCurso.bind(this);
    this.modificar = this.modificar.bind(this);
    this.verinfo = this.verinfo.bind(this);
  }

  //Indicador para pasar a la vista de crear curso
  iraCreaCurso(){
    this.setState({
      iraNuevoCurso : true
    })
  }

  //Id que modificar
  modificar(cursoId){
    this.setState({
      modificarId: cursoId
    })
  }
  verinfo(cursoId, nombre){
    this.setState({
      verInfoId: cursoId,
      cursoNombre: nombre
    })
  }

  render() {
    //Si se pulsa el boton "nuevo curso"
    if (this.state.iraNuevoCurso === true) {
      //Redirigimos a la pagina alumno/crea
      return <Redirect to='/cursos/crea'  />
    }
    //Si se pulsa "editar curso", el state.modificarId existe
    if(this.state.modificarId){
      return <Redirect  to={'/cursos/crea/'+this.state.modificarId} />
    }
    if(this.state.verInfoId){
      return <Redirect  to={
        {
          pathname: '/curso/'+this.state.verInfoId,
          state: {titulo: this.state.cursoNombre}
        }
      } />
    }
    
    //Cursos que has creado en array
    let cursos = this.props.cursos

    //Donde se metera cada curso
    let cursostr = [];
    
    //Guardas el this
    let that = this;
    
    //Recorremos cursos
    cursos.forEach( function(curso, indice, array) {
      cursostr.push(
        <tr key={curso._id}>
          <th scope="row">{curso._id}</th>
          <td>{curso.nombre}</td>
          <td>{curso.especialidad}</td>
          <td><i className='fa fa-lg fa-trash borrar' onClick={()=>that.props.onBorrar(curso._id)}></i></td>
          <td><i className='fa fa-lg fa-edit modificar' onClick={()=>that.modificar(curso._id)}></i></td>
          <td><i className='fa fa-lg fa-eye ver' onClick={()=>that.verinfo(curso._id, curso.nombre)}></i></td>
        </tr>)
    });
    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>Id curso</th>
              <th>Nombre</th>
              <th>Especialidad</th>
            </tr>
          </thead>
          <tbody>
            {cursostr}
          </tbody>
        </Table>
        <br />
        <Button color="danger" onClick={this.iraCreaCurso}>Nuevo</Button>
      </div>
    );
  }
}

export default Cursos;
