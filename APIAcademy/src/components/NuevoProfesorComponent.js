import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap';

class NuevoProfesor extends Component {

  constructor(props) {
    super(props);
    //Estado por defecto
    //Si tiene elementos en cursos muestra el primero sino en blanco
    let estado = {
      genero: 'Hombre',
      nombre: '',
      email: '',
      edad: '18',
      curso: (this.props.cursos.length > 0)? this.props.cursos[0].nombre: ''
    }
    
    //Miramos si existen datos, primero los parametros y luego alumnos
    if(this.props.match && this.props.profesores){
      //Guarda id que pasamos
      let idProfesorEditar = this.props.match.params.profesorId;
      //Filtro para obtener datos del alumno a editar
      estado = this.props.profesores.filter((item)=>(item._id===idProfesorEditar))[0];
    }

    //irAlumnos para redirigir a listado de alumnos una vez enviados los datos del form
    estado.irProfesores=false;

    //Asignamos al state nuestro estado por defecto
    this.state = estado;

    //Referenciamos al metodo
    this.mirarCambioInput = this.mirarCambioInput.bind(this);
    this.enviar = this.enviar.bind(this);
  }

   //Metodo que notara los cambios de cualquier input
   mirarCambioInput(event) {
    //En target guardamos toda la etiqueta input
    const target = event.target;
    
    //Aqui se guarda el tipo de input, si es checkbox mira el valor
    const value = target.type === 'checkbox' ? target.checked : target.value;
    //Coge el valor name del input
    const name = target.name;

    //Como hemos puesto el valor name igual que el nombre de las propiedades a cambiar las modificamos pasandole las constantes
    this.setState({
      [name]: value
    });
  }

  //Redigir el contenido de State a props "onChange" que nos hayan facilitado
  enviar(evt) {
    //Envia el this.state, al metodo onEnviar pasandolo al App.js
    this.props.onEnviar(this.state);
    //Evita que el formulario se comporte como por defecto
    evt.preventDefault();
    //Cambio la propiedad state irCursos a true para mirar de cambiar la pagina
    this.setState({irProfesores: true});
  }

  render() {
    
    //Cursos que has creado en array
    let cursos = this.props.cursos

    //Donde se metera cada curso
    let opcion = cursos.map(
      (curso)=><option key={curso._id}>{curso.nombre}</option>
    );

    //Si es true cambio a la lista de profesores
    if(this.state.irProfesores === true){
      return <Redirect to='/profesores' />
    }
    return (
      <Form onSubmit={this.enviar}>
        <FormGroup>
          <Label for="exampleNombre">Nombre</Label>
          <Input type="text" name="nombre" id="exampleNombre" placeholder="Nombre del profesor" value={this.state.nombre}  onChange={this.mirarCambioInput} />
        </FormGroup>
        <FormGroup>
          <Label for="exampleEmail">Email</Label>
          <Input type="email" name="email" id="exampleEmail" placeholder="Email del profesor" value={this.state.email}  onChange={this.mirarCambioInput} />
        </FormGroup>
        <Row form>
          <Col md={6}>
          <FormGroup>
            <Label for="exampleEdad">Edad</Label>
            <Input type="text" name="edad" id="exampleEdad" placeholder="18" value={this.state.edad}  onChange={this.mirarCambioInput} />
          </FormGroup>
          </Col>
          <Col md={6}>
          <FormGroup>
            <Label for="exampleGenero">Género</Label>
            <Input type="select" name="genero" id="exampleGenero" value={this.state.genero}  onChange={this.mirarCambioInput}>
              <option>Hombre</option>
              <option>Mujer</option>
            </Input>
          </FormGroup>
          </Col>
        </Row>
        {/* Select del listado de curso */}
        <FormGroup>
          <Label for="exampleCurso">Curso</Label>
          <Input type="select" name="curso" id="exampleCurso" value={this.state.curso} onChange={this.mirarCambioInput}>
            {opcion}
          </Input>
        </FormGroup>

        <Button color="primary">Enviar</Button>
      </Form>
    );
  }
}

export default NuevoProfesor;