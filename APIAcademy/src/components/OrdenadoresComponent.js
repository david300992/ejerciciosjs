import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Table, Button } from 'reactstrap';

class Ordenadores extends Component {
  constructor(props) {
    super(props);

    //Estados para ver si vamos a nuevo alumno y el id que modificaremos
    this.state = {
      iraNuevoOrdenador: false,
      modificarId: null
    }

    //Referencias a las funciones
    this.iraCreaOrdenador = this.iraCreaOrdenador.bind(this);
    this.modificar = this.modificar.bind(this);
  }

  //Indicador para pasar a la vista de crear ordenador
  iraCreaOrdenador(){
    this.setState({
      iraNuevoOrdenador : true
    })
  }

  //Id que modificar
  modificar(ordenadorId){
    this.setState({
      modificarId: ordenadorId
    })
  }

  render() {
    //Si se pulsa el boton "nuevo ordenador"
    if (this.state.iraNuevoOrdenador === true) {
      //Redirigimos a la pagina alumno/crea
      return <Redirect to='/ordenadores/crea'  />
    }
    //Si se pulsa "editar ordenador", el state.modificarId existe
    if(this.state.modificarId){
      return <Redirect  to={'/ordenadores/crea/'+this.state.modificarId} />
    }
    
    //Cursos que has creado en array
    let ordenadores = this.props.ordenadores

    //Donde se metera cada curso
    let ordenadorestr = [];
    
    //Guardas el this
    let that = this;
    
    //Recorremos cursos
    ordenadores.forEach( function(ordenador, indice, array) {
        ordenadorestr.push(
        <tr key={ordenador._id}>
          <th scope="row">{ordenador._id}</th>
          <td>{ordenador.marca}</td>
          <td>{ordenador.modelo}</td>
          <td>{ordenador.alumno}</td>
          <td><i className='fa fa-lg fa-trash borrar' onClick={()=>that.props.onBorrar(ordenador._id)}></i></td>
          <td><i className='fa fa-lg fa-edit modificar' onClick={()=>that.modificar(ordenador._id)}></i></td>
        </tr>)
    });
    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>Id ordenador</th>
              <th>Marca</th>
              <th>Modelo</th>
              <th>Alumno</th>
            </tr>
          </thead>
          <tbody>
            {ordenadorestr}
          </tbody>
        </Table>
        <br />
        <Button color="danger" onClick={this.iraCreaOrdenador}>Nuevo</Button>
      </div>
    );
  }
}

export default Ordenadores;
