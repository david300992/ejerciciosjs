export default class Api {

    //Carga inicial
    static getInicial(callback) {
        let alumnos, cursos, profesores, ordenadores;
        fetch('http://localhost:3001/alumnos/especial')
            .then(results => results.json())
            .then(data => {
                alumnos = data;
            })
            .then(() => {
                return fetch('http://localhost:3001/cursos');
            })
            .then(results => results.json())
            .then(data => {
                cursos = data;
                //callback({ alumnos, cursos });
            })
            .then(()=> {
                return fetch('http://localhost:3001/profesores');
            })
            .then(results => results.json())
            .then(data =>{
                profesores = data;
                //callback({ alumnos, cursos, profesores });
            })
            .then(()=> {
                return fetch('http://localhost:3001/ordenadores');
            })
            .then(results => results.json())
            .then(data =>{
                ordenadores = data;
                callback({ alumnos, cursos, profesores, ordenadores });
            })
            .catch(err => callback(false));
    }

    /*** ALUMNO ***/
    //Recibe el objeto alumno
    static nuevoAlumno(obData) {
        delete (obData.id); //no interesa a bdd
        let dataBody = [];
        //Recorre el objeto y lo va poniendo en una linea
        for (var property in obData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(obData[property]);
            dataBody.push(encodedKey + "=" + encodedValue);
        }
        dataBody = dataBody.join("&");
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/alumnos', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: dataBody
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                //resp es obj
                console.log(resp);
            })
    }
    //Recibe el objeto alumno
    static borraAlumno(obData) {
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/alumnos/'+obData, {
            method: 'DELETE',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' })
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
            })
    }
    //Recibe el objeto alumno
    static editaAlumno(obData, callback) {
        delete (obData.id); //no interesa a bdd
        let dataBody = [];
        //Recorre el objeto y lo va poniendo en una linea
        for (var property in obData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(obData[property]);
            dataBody.push(encodedKey + "=" + encodedValue);
        }
        dataBody = dataBody.join("&");
        
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/alumnos/'+obData._id, {
            method: 'put',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: dataBody
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
                callback();
            })
    }

    /*** CURSO ***/
    //Recibe el objeto curso
    static nuevoCurso(obData) {
        delete (obData.id); //no interesa a bdd
        let dataBody = [];
        //Recorre el objeto y lo va poniendo en una linea
        for (var property in obData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(obData[property]);
            dataBody.push(encodedKey + "=" + encodedValue);
        }
        dataBody = dataBody.join("&");
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/cursos', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: dataBody
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
            })
    }
    //Recibe el objeto curso
    static borraCurso(obData) {

        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/cursos/'+obData, {
            method: 'DELETE',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' })
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
            })
    }
    //Recibe el objeto alumno
    static editaCurso(obData, callback) {
        delete (obData.id); //no interesa a bdd
        let dataBody = [];
        //Recorre el objeto y lo va poniendo en una linea
        for (var property in obData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(obData[property]);
            dataBody.push(encodedKey + "=" + encodedValue);
        }
        dataBody = dataBody.join("&");
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/cursos/'+obData._id, {
            method: 'put',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: dataBody
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
                callback();
            })
    }
    //Recibe el objeto curso
    static old_verInfoCurso(obData) {
        let infoCurso;
        delete (obData.id); //no interesa a bdd
        let dataBody = [];
        //Recorre el objeto y lo va poniendo en una linea
        for (var property in obData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(obData[property]);
            dataBody.push(encodedKey + "=" + encodedValue);
        }
        dataBody = dataBody.join("&");
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/cursos/'+obData+'/alumnos', {
            method: 'get',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: dataBody
        })
            //Pasa el resultado a json
            .then(results => results.json())
            //Una vez pasado a json lo devuelve por consola
            .then((data) => {
                console.log(data);
                infoCurso= data
                console.log(infoCurso)
            })
    }

    static verInfoCurso(cursoId, callback) {
          //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/cursos/'+cursoId+'/alumnos')
            //Pasa el resultado a json
            .then(results => results.json())
            //Una vez pasado a json lo devuelve por consola
            .then((data) => {
                callback(data);
            })
    }
    


    /*** ORDENADOR ***/
    //Recibe el objeto curso
    static nuevoOrdenador(obData) {
        delete (obData.id); //no interesa a bdd
        let dataBody = [];
        //Recorre el objeto y lo va poniendo en una linea
        for (var property in obData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(obData[property]);
            dataBody.push(encodedKey + "=" + encodedValue);
        }
        dataBody = dataBody.join("&");
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/ordenadores', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: dataBody
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
            })
    }
    //Recibe el objeto curso
    static borraOrdenador(obData) {
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/ordenadores/'+obData, {
            method: 'DELETE',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' })
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
            })
    }
    static editaOrdenador(obData, callback) {
        delete (obData.id); //no interesa a bdd
        console.log(obData)
        let dataBody = [];
        //Recorre el objeto y lo va poniendo en una linea
        for (var property in obData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(obData[property]);
            dataBody.push(encodedKey + "=" + encodedValue);
        }
        dataBody = dataBody.join("&");
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/ordenadores/'+obData._id, {
            method: 'put',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: dataBody
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
                callback();
            })
    }

    /*** PROFESOR ***/
    //Recibe el objeto curso
    static nuevoProfesor(obData) {
        delete (obData.id); //no interesa a bdd
        let dataBody = [];
        //Recorre el objeto y lo va poniendo en una linea
        for (var property in obData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(obData[property]);
            dataBody.push(encodedKey + "=" + encodedValue);
        }
        dataBody = dataBody.join("&");
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/profesores', {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: dataBody
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
            })
    }
    //Recibe el objeto curso
    static borraProfesor(obData) {
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/profesores/'+obData, {
            method: 'DELETE',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' })
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
            })
    }
    static editaProfesor(obData, callback) {
        delete (obData.id); //no interesa a bdd
        let dataBody = [];
        //Recorre el objeto y lo va poniendo en una linea
        for (var property in obData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(obData[property]);
            dataBody.push(encodedKey + "=" + encodedValue);
        }
        dataBody = dataBody.join("&");
        //Indica que ruta de la api pide y como lo pide
        fetch('http://localhost:3001/profesores/'+obData._id, {
            method: 'put',
            headers: new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
            body: dataBody
        })
            //Pasa el resultado a json
            .then(resp => resp.json())
            //Una vez pasado a json lo devuelve por consola
            .then((resp) => {
                console.log(resp);
                callback();
            })
    }

}