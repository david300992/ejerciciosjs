import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Table, Button } from 'reactstrap';

class Alumnos extends Component {
  constructor(props) {
    super(props);

    //Estados para ver si vamos a nuevo alumno y el id que modificaremos
    this.state = {
      iraNuevoAlumno: false,
      modificarId: null
    }

    //Referencias a las funciones
    this.iraCreaAlumno = this.iraCreaAlumno.bind(this);
    this.modificar = this.modificar.bind(this);
  }

  //Indicador para pasar a la vista de crear alumno
  iraCreaAlumno(){
    this.setState({
      iraNuevoAlumno : true
    })
  }

  //Id que modificar
  modificar(alumnoId){
    this.setState({
      modificarId: alumnoId
    })
  }

  render() {
    //Si se pulsa el boton "nuevo alumno"
    if (this.state.iraNuevoAlumno === true) {
      //Redirigimos a la pagina alumno/crea
      return <Redirect to='/alumnos/crea'  />
    }
    //Si se pulsa "editar alumno", el state.modificarId existe
    if(this.state.modificarId){
      return <Redirect  to={'/alumnos/crea/'+this.state.modificarId} />
    }

    //Cursos que has creado en array
    let alumnos = this.props.alumnos

    //Donde se metera cada alumno
    let alumnostr = [];
    
    //Guardas el this
    let that = this;
    
    //Recorremos alumnos
    alumnos.forEach( function(alumno, indice, array) {
      alumnostr.push(
      <tr key={alumno._id}>
        <th scope="row">{alumno._id}</th>
        <td>{alumno.nombre}</td>
        <td>{alumno.email}</td>
        <td>{alumno.edad}</td>
        <td>{alumno.genero}</td>
        <td>{(alumno.datoscurso && alumno.datoscurso.length) ? alumno.datoscurso[0].nombre : ""}</td>
        <td><i className='fa fa-lg fa-trash borrar' onClick={()=>that.props.onBorrar(alumno._id)}></i></td>
        <td><i className='fa fa-lg fa-edit modificar' onClick={()=>that.modificar(alumno._id)}></i></td>
      </tr>)
    });

    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>Id alumno</th>
              <th>Nombre</th>
              <th>Email</th>
              <th>Edad</th>
              <th>Genero</th>
              <th>Curso</th>
            </tr>
          </thead>
          <tbody>
            {alumnostr}
          </tbody>
        </Table>
        <br />
        <Button color="danger" onClick={this.iraCreaAlumno}>Nuevo</Button>
      </div>
    );
  }
}

export default Alumnos;
