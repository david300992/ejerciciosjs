import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

class NuevoOrdenador extends Component {

  constructor(props) {
    super(props);
    //Estado por defecto
    //Si tiene elementos en cursos muestra el primero sino en blanco
    let estado = {
      marca: '',
      modelo: '',
      alumno: (this.props.alumnos.length > 0)? this.props.alumnos[0].nombre: ''
    }
    
    //Miramos si existen datos, primero los parametros y luego alumnos
    if(this.props.match && this.props.ordenadores){
      //Guarda id que pasamos
      let idOrdenadorEditar = this.props.match.params.ordenadorId;
      //Filtro para obtener datos del alumno a editar
      estado = this.props.ordenadores.filter((item)=>(item._id===idOrdenadorEditar))[0];
    }

    //irAlumnos para redirigir a listado de alumnos una vez enviados los datos del form
    estado.irOrdenadores=false;

    //Propiedades por cada valor del formulario
    /*
    this.state = {
      genero: 'Hombre',
      nombre: '',
      email: '',
      edad: '18',
      irAlumnos: false,
    };
    */
    //Asignamos al state nuestro estado por defecto
    this.state = estado;

    //Referenciamos al metodo
    this.mirarCambioInput = this.mirarCambioInput.bind(this);
    this.enviar = this.enviar.bind(this);
  }

   //Metodo que notara los cambios de cualquier input
   mirarCambioInput(event) {
    //En target guardamos toda la etiqueta input
    const target = event.target;
    
    //Aqui se guarda el tipo de input, si es checkbox mira el valor
    const value = target.type === 'checkbox' ? target.checked : target.value;
    //Coge el valor name del input
    const name = target.name;

    //Como hemos puesto el valor name igual que el nombre de las propiedades a cambiar las modificamos pasandole las constantes
    this.setState({
      [name]: value
    });
  }

  //Redigir el contenido de State a props "onChange" que nos hayan facilitado
  enviar(evt) {
    //Evita que el formulario se comporte como por defecto
    evt.preventDefault();
    //Envia el this.state, al metodo onEnviar pasandolo al App.js
    this.props.onEnviar(this.state);
    
    //Cambio la propiedad state irCursos a true para mirar de cambiar la pagina
    this.setState({irOrdenadores: true});
  }

  render() {
    
    //Cursos que has creado en array
    let alumnos = this.props.alumnos

    //Donde se metera cada curso
    let opcion = alumnos.map(
      (alumno)=><option key={alumno._id}>{alumno.nombre}</option>
    );

    //Si es true cambio a la lista de alumnos
    if(this.state.irOrdenadores === true){
      return <Redirect to='/ordenadores' />
    }
    return (
      <Form onSubmit={this.enviar}>
        <FormGroup>
          <Label for="exampleMarca">Marca</Label>
          <Input type="text" name="marca" id="exampleMarca" placeholder="Marca del ordenador" value={this.state.marca}  onChange={this.mirarCambioInput} />
        </FormGroup>
        <FormGroup>
          <Label for="exampleModelo">Modelo</Label>
          <Input type="text" name="modelo" id="exampleModelo" placeholder="Modelo del ordenador" value={this.state.modelo}  onChange={this.mirarCambioInput} />
        </FormGroup>
        {/* Select del listado de curso */}
        <FormGroup>
          <Label for="exampleAlumno">Alumno</Label>
          <Input type="select" name="alumno" id="exampleAlumno" value={this.state.alumno} onChange={this.mirarCambioInput}>
            {opcion}
          </Input>
        </FormGroup>
        <Button color="primary">Enviar</Button>
      </Form>
    );
  }
}

export default NuevoOrdenador;