import React, { Component } from 'react';
import { Table } from 'reactstrap';
import API from './Api';

class InfoCursos extends Component {
  constructor(props) {
    super(props);
    let lacosa = this.props.match.params.cursoId;
    API.verInfoCurso(lacosa, (data) => this.setState({
        alumnos: data
    }));
    this.state = {alumnos: 0};
  }


  render() {
    //Cursos que has creado en array
    //Mientras no carga allumnos
    if (this.state.alumnos===0) return <h3>Cargando...</h3>;
    //Cuando no hay alumnos
    if(this.state.alumnos.length === 0) return <h3> No hay datos</h3>;
    let linAlumnos = this.state.alumnos.map((al)=>
          <tr key={al._id}>
          <th scope="row">{al._id}</th>
          <td>{al.nombre}</td>
          <td>{al.email}</td>
        </tr>
  )
    return (
      <div>
        <h1>{this.props.location.state.titulo}</h1>
        <Table>
          <thead>
            <tr>
              <th>Id alumno</th>
              <th>Nombre alumno</th>
              <th>Correo</th>
            </tr>
          </thead>
          <tbody>
            {linAlumnos}
          
          </tbody>
        </Table>
      </div>
    );
  }
}

export default InfoCursos;
