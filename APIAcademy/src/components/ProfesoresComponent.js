import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Table, Button } from 'reactstrap';

class Profesores extends Component {
  constructor(props) {
    super(props);

    //Estados para ver si vamos a nuevo alumno y el id que modificaremos
    this.state = {
      iraNuevoProfesor: false,
      modificarId: null
    }

    //Referencias a las funciones
    this.iraCreaProfesor = this.iraCreaProfesor.bind(this);
    this.modificar = this.modificar.bind(this);
  }

  //Indicador para pasar a la vista de crear alumno
  iraCreaProfesor(){
    this.setState({
      iraNuevoProfesor : true
    })
  }

  //Id que modificar
  modificar(profesorId){
    this.setState({
      modificarId: profesorId
    })
  }

  render() {
    //Si se pulsa el boton "nuevo profesor"
    if (this.state.iraNuevoProfesor === true) {
      //Redirigimos a la pagina profesor/crea
      return <Redirect to='/profesores/crea'  />
    }
    //Si se pulsa "editar profesor", el state.modificarId existe
    if(this.state.modificarId){
      return <Redirect  to={'/profesores/crea/'+this.state.modificarId} />
    }

    //Cursos que has creado en array
    let profesores = this.props.profesores

    //Donde se metera cada alumno
    let profesorestr = [];
    
    //Guardas el this
    let that = this;
    //Recorremos alumnos
    profesores.forEach( function(profesor, indice, array) {
        profesorestr.push(
      <tr key={profesor._id}>
        <th scope="row">{profesor._id}</th>
        <td>{profesor.nombre}</td>
        <td>{profesor.email}</td>
        <td>{profesor.edad}</td>
        <td>{profesor.genero}</td>
        <td>{profesor.curso}</td>
        <td><i className='fa fa-lg fa-trash borrar' onClick={()=>that.props.onBorrar(profesor._id)}></i></td>
        <td><i className='fa fa-lg fa-edit modificar' onClick={()=>that.modificar(profesor._id)}></i></td>
      </tr>)
    });

    return (
      <div>
        <Table>
          <thead>
            <tr>
              <th>Id profesor</th>
              <th>Nombre</th>
              <th>Email</th>
              <th>Edad</th>
              <th>Genero</th>
              <th>Curso</th>
            </tr>
          </thead>
          <tbody>
            {profesorestr}
          </tbody>
        </Table>
        <br />
        <Button color="danger" onClick={this.iraCreaProfesor}>Nuevo</Button>
      </div>
    );
  }
}

export default Profesores;
