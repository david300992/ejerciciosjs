const ObjectId = require('mongodb').ObjectID;

//db base de datos (academy)
//document documento, dato a guardar
//collection (cursos, alumnos...)
//callback función que se llamará con el dato devuelto
exports.insertDocument = (db, document, collection) => {
    const coll = db.collection(collection);
    // insert inserta el documento y ejecuta la función callback
    // pasándole el posible error y el resultado si ok
    return coll.insert(document);
};

// consulta los documentos (registros) de la colección facilitada
// se devuelven via la callback
exports.findDocuments = (db, collection) => {
    const coll = db.collection(collection);
    return coll.find({}).toArray();
};


//Busca aplicando un filtro
exports.findDocumentsFilter = (db, filter, collection) => {
    const coll = db.collection(collection);
    return coll.find(filter).toArray();
};

//Elimina documentos que cumplen filtro /condicion
exports.delDocuments = (db, filter, collection) => {
    const coll = db.collection(collection);
    return coll.remove(filter);
};

//Elimina todo los documentos de una coleccion
exports.delCollection = (db, collection) => {
    const coll = db.collection(collection);
    return coll.remove();
};

//Elimina un documento dado por id
exports.delOneDocument = (db, collection, documentId) => {
    const coll = db.collection(collection);
    return coll.remove({"_id": ObjectId(documentId)});
};

exports.findOneDocument = (db, collection, documentId) => {
    const coll = db.collection(collection);
    return coll.find({"_id": ObjectId(documentId)}).toArray();
};


// facilitamos el documento a eliminar
exports.removeDocument = (db, document, collection) => {
    const coll = db.collection(collection);
    return coll.deleteOne(document);
};

// facilitamos el documento a modificar
// los cambios en update
exports.updateDocument = (db, document, update, collection) => {
    const coll = db.collection(collection);
    return coll.updateOne({"_id": ObjectId(document)}, { $set: update }, null);
};