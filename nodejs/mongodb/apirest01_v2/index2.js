
// requeriments
const http = require('http');
const express = require('express');
// const morgan = require('morgan');
const bodyParser = require('body-parser');
const cursoRouter = require('./routes/cursoRouter');

//constants
const hostname = 'localhost';
const port = 3000;

//iniciem app express
const app = express();
// app.use('/cursos', cursoRouter);
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));


app.use(bodyParser.json());

app.all('/cursos', (req,res,next) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  next();
});

app.get('/cursos', (req,res,next) => {
    res.end('Will send all the cursos to you!');
});

app.post('/cursos', (req, res, next) => {
 res.end('Will add the dish: ' + req.body.nombre + ' with details: ' + req.body.descripcion);
});

app.put('/cursos', (req, res, next) => {
  res.statusCode = 403;
  res.end('PUT operation not supported on /cursos');
});
 
app.delete('/cursos', (req, res, next) => {
    res.end('Deleting all cursos');
});

app.get('/cursos/:dishId', (req,res,next) => {
    res.end('Will send details of the dish: ' + req.params.dishId +' to you!');
});

app.post('/cursos/:dishId', (req, res, next) => {
  res.statusCode = 403;
  res.end('POST operation not supported on /cursos/'+ req.params.dishId);
});

app.put('/cursos/:dishId', (req, res, next) => {
  res.write('Updating the dish: ' + req.params.dishId + '\n');
  res.end('Will update the dish: ' + req.body.nombre + 
        ' with details: ' + req.body.descripcion);
});

app.delete('/cursos/:dishId', (req, res, next) => {
    res.end('Deleting dish: ' + req.params.dishId);
});



const server = http.createServer(app);


// app.use((req, res, next) => {
//     console.log(req.headers);
//     res.statusCode = 200;
//     res.setHeader('Content-Type', 'text/html');
//     res.end('<html><body><h1>Hola Express!</h1></body></html>');
//   });




server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
