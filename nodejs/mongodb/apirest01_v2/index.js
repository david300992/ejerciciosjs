// requeriments
const express = require('express');
const morgan = require('morgan');
const cursoRouter = require('./routes/cursoRouter');
const alumnoRouter = require('./routes/alumnoRouter');
//Añadido
const ordenadorRouter = require('./routes/ordenadorRouter');
const profesorRouter = require('./routes/profesorRouter');

//constants
const hostname = 'localhost';
const port = 3001;

//iniciem app express
const app = express();
app.use(morgan('dev'));

//Solo funcionara /cursos y /alumnos
app.use('/cursos', cursoRouter);
app.use('/alumnos', alumnoRouter);
//Añadido
app.use('/ordenadores', ordenadorRouter);
app.use('/profesores', profesorRouter);

app.listen(port, hostname, () => {
  console.log(`Servidor corriendo por http://${hostname}:${port}/`);
});