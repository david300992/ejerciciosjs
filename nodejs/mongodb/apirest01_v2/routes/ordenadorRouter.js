const express = require('express');
const bodyParser = require('body-parser');

const modelRouter = express.Router();

/////////
const MongoClient = require('mongodb').MongoClient;
const dboper = require('../operations');
const url = 'mongodb://localhost:27017/academy';
const dbname = 'academy';
/////////



//--------- 
const model = 'ordenador';
const models = 'ordenadores';
//-----------------------

// modelRouter.use(bodyParser.json());
modelRouter.use(bodyParser.urlencoded({
    extended: true
}));

//Corresponde a /ordenadores
modelRouter.route('/')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'text/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req,res,next)=>{
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })
    //Muestra todos los ordenadores
    .get((req, res, next) => {
        // conexión con base de datos para obtener ordenadores
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                console.log('Conectado al servidor');
                const db = client.db(dbname);
                dboper.findDocuments(db, "ordenadores")
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => {res.end(JSON.stringify(err))});

            }).catch((err) => {res.end(JSON.stringify(err))});
    })
    //Crearemos un ordenador nuevo
    .post((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                let nuevoOrdenador = {
                    marca: req.body.marca,
                    modelo: req.body.modelo,
                    alumno: req.body.alumno
                }
                dboper.insertDocument(db, nuevoOrdenador, 'ordenadores')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borraremos todos los ordenadores
    .delete((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                dboper.delCollection(db, 'ordenadores')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    });

//Corresponde a /ordenadores/:idField
modelRouter.route('/:idField')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'text/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req,res,next)=>{
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })    
    //Mostrara la informacion del curso dado
    .get((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                console.log('registro:' + req.params.idField);
                const db = client.db(dbname);
                dboper.findOneDocument(db, "ordenadores", req.params.idField)
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => {res.end(JSON.stringify(err))});

            }).catch((err) => {res.end(JSON.stringify(err))});
    })
    //Modificamos el ordenador con el idField selecionado
    .put((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Objeto donde ponemos lo que vamos a modificar
                let nuevoOrdenador = {
                    marca: req.body.marca,
                    modelo: req.body.modelo,
                    alumno: req.body.alumno
                }
                //Pasamos el parametro, el objeto y cursos
                dboper.updateDocument(db, req.params.idField, nuevoOrdenador, 'ordenadores')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borramos el ordenador con el idField selecionado
    .delete((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Pasamos el parametro, el objeto y cursos
                dboper.delOneDocument(db, 'ordenadores', req.params.idField)
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    });


module.exports = modelRouter;
