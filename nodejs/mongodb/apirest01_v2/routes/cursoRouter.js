
const express = require('express');
const bodyParser = require('body-parser');

const modelRouter = express.Router();
const ObjectID = require('mongodb').ObjectID;

/////////
const MongoClient = require('mongodb').MongoClient;
const dboper = require('../operations');
const url = 'mongodb://localhost:27017/academy';
const dbname = 'academy';
/////////



//--------- 
const model = 'curso';
const models = 'cursos';
//-----------------------

// modelRouter.use(bodyParser.json());
//Recibe los datos en este formato
modelRouter.use(bodyParser.urlencoded({
    extended: true
}));

//Corresponde a /cursos
modelRouter.route('/')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'text/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req,res,next)=>{
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })    
    //Muestra todos los cursos
    .get((req, res, next) => {
        // conexión con base de datos para obtener cursos
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                console.log('Conectado al servidor');
                const db = client.db(dbname);
                dboper.findDocuments(db, "cursos")
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Crearemos un curso nuevo
    .post((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                let nuevoCurso = {
                    nombre: req.body.nombre,
                    especialidad: req.body.especialidad
                }
                dboper.insertDocument(db, nuevoCurso, 'cursos')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borraremos todos los cursos
    .delete((req, res, next) => {
        //res.end(`Deleting all the ${models}!`);
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                console.log('registro:' + req.params.idField);
                const db = client.db(dbname);
                dboper.delCollection(db, 'cursos')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    });

//Corresponde a /cursos/:idField
modelRouter.route('/:idField')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'appliction/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req,res,next)=>{
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })
    //Mostrara la informacion del curso dado
    .get((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                console.log('registro:' + req.params.idField);
                const db = client.db(dbname);
                dboper.findOneDocument(db, "cursos", req.params.idField)
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });

            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Modificamos el curso con el idField selecionado
    .put((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Objeto donde ponemos lo que vamos a modificar
                let nuevoCurso = {
                    nombre: req.body.nombre,
                    especialidad: req.body.especialidad
                }
                //Pasamos el parametro, el objeto y cursos
                dboper.updateDocument(db, req.params.idField, nuevoCurso, 'cursos')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borramos el cursos con el idField selecionado
    .delete((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Pasamos el parametro, el objeto y cursos
                dboper.delOneDocument(db, 'cursos', req.params.idField)
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    });

//Corresponde a /cursos/:idField/alumnos pasandole el id del curso
modelRouter.route('/:idField/alumnos')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'appliction/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req,res,next)=>{
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })
    //Mostrara los alumnos del curso dado
    .get((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
        .then((client) => {
            const db = client.db(dbname);
            //Objeto donde ponemos lo que vamos a modificar
            let muestraAlumnosCurso = {
                curso: ObjectID(req.params.idField)
            }
            //Pasamos el parametro, el objeto y cursos
            dboper.findDocumentsFilter(db, muestraAlumnosCurso, 'alumnos')
                .then((docs) => {
                    client.close();
                    res.end(JSON.stringify(docs));
                })
                .catch((err) => { res.end(JSON.stringify(err)) });
        }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borramos todos los alumnos del cursos
    .delete((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Objeto donde ponemos lo que vamos a modificar
                let borraAlumnosCurso = {
                    curso: req.params.idField
                }
                //Pasamos el objeto y la colecion alumnos
                dboper.delDocuments(db, borraAlumnosCurso, 'alumnos')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    });

    module.exports = modelRouter;