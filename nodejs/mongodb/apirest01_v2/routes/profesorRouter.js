const express = require('express');
const bodyParser = require('body-parser');

const modelRouter = express.Router();

/////////
const MongoClient = require('mongodb').MongoClient;
const dboper = require('../operations');
const url = 'mongodb://localhost:27017/academy';
const dbname = 'academy';
/////////



//--------- 
const model = 'profesor';
const models = 'profesores';
//-----------------------

// modelRouter.use(bodyParser.json());
modelRouter.use(bodyParser.urlencoded({
    extended: true
}));

//Corresponde a /profesores
modelRouter.route('/')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'text/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req,res,next)=>{
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })
    //Muestra todos los profesores
    .get((req, res, next) => {
        // conexión con base de datos para obtener profesores
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                console.log('Conectado al servidor');
                const db = client.db(dbname);
                dboper.findDocuments(db, "profesores")
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => {res.end(JSON.stringify(err))});

            }).catch((err) => {res.end(JSON.stringify(err))});
    })
    //Crearemos un profesor nuevo
    .post((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                let nuevoProfesor = {
                    nombre: req.body.nombre,
                    email: req.body.email,
                    edad: req.body.edad,
                    genero: req.body.genero,
                    curso: req.body.curso,
                }
                dboper.insertDocument(db, nuevoProfesor, 'profesores')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borraremos todos los profesores
    .delete((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                dboper.delCollection(db, 'profesores')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    });

//Corresponde a /profesores/:idField
modelRouter.route('/:idField')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'text/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req,res,next)=>{
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })
    //Mostrara la informacion del profesor dado
    .get((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                console.log('registro:' + req.params.idField);
                const db = client.db(dbname);
                dboper.findOneDocument(db, "profesores", req.params.idField)
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => {res.end(JSON.stringify(err))});

            }).catch((err) => {res.end(JSON.stringify(err))});
    })
    //Modificamos el profesor con el idField selecionado
    .put((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Objeto donde ponemos lo que vamos a modificar
                let nuevoProfesor = {
                    nombre: req.body.nombre,
                    email: req.body.email,
                    edad: req.body.edad,
                    genero: req.body.genero,
                    curso: req.body.curso,
                }
                //Pasamos el parametro, el objeto y cursos
                dboper.updateDocument(db, req.params.idField, nuevoProfesor, 'profesores')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borramos el profesor con el idField selecionado
    .delete((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Pasamos el parametro, el objeto y cursos
                dboper.delOneDocument(db, 'profesores', req.params.idField)
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    });


module.exports = modelRouter;
