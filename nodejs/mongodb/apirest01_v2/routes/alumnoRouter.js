const ObjectId = require('mongodb').ObjectID;

const express = require('express');
const bodyParser = require('body-parser');

const modelRouter = express.Router();

/////////
const MongoClient = require('mongodb').MongoClient;
const dboper = require('../operations');
const url = 'mongodb://localhost:27017/academy';
const dbname = 'academy';
/////////

//--------- 
const model = 'alumno';
const models = 'alumnos';
//-----------------------

// modelRouter.use(bodyParser.json());
modelRouter.use(bodyParser.urlencoded({
    extended: true
}));

//Corresponde a /alumnos
modelRouter.route('/')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'application/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req, res, next) => {
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })
    //Muestra todos los alumnos
    .get((req, res, next) => {
        // conexión con base de datos para obtener alumnos
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                console.log('Conectado al servidor');
                const db = client.db(dbname);
                dboper.findDocuments(db, "alumnos")
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });

            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Crearemos un alumno nuevo
    .post((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                let nuevoAlumno = {
                    nombre: req.body.nombre,
                    email: req.body.email,
                    edad: req.body.edad,
                    genero: req.body.genero,
                    curso: (req.body.curso) ? new ObjectId(req.body.curso) : ""
                }
                dboper.insertDocument(db, nuevoAlumno, 'alumnos')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borraremos todos los alumnos
    .delete((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                dboper.delCollection(db, 'alumnos')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    });


//Corresponde a /alumnos
modelRouter.route('/especial')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'application/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req, res, next) => {
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })
    //Muestra todos los alumnos
    .get((req, res, next) => {
        // conexión con base de datos para obtener alumnos
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                console.log('Conectado al servidor');
                const db = client.db(dbname);
                const coll = db.collection("alumnos");
                //A la coleccion, le agrega los datos del $lookup=join
                //de la tabla cursos, el campo curso de  la tabla alumnos
                //con el _id de la tabla cursos
                coll.aggregate([
                    {
                        $lookup:
                            {
                                from: "cursos",
                                localField: "curso",
                                foreignField: "_id",
                                as: "datoscurso"
                            }
                    }]).toArray()
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });

            }).catch((err) => { res.end(JSON.stringify(err)) });
    })


modelRouter.route('/:idField')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'applictiona/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req, res, next) => {
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })
    //Mostrara la informacion del alumno dado
    .get((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                dboper.findOneDocument(db, "alumnos", req.params.idField)
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });

            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Modificamos el alumno con el idField selecionado
    .put((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Objeto donde ponemos lo que vamos a modificar
                let nuevoAlumno = {
                    nombre: req.body.nombre,
                    email: req.body.email,
                    edad: req.body.edad,
                    genero: req.body.genero,
                    curso: (req.body.curso) ? new ObjectId(req.body.curso) : ""

                }

                //Pasamos el parametro, el objeto y cursos
                dboper.updateDocument(db, req.params.idField, nuevoAlumno, 'alumnos')
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borramos el alumno con el idField selecionado
    .delete((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Pasamos el parametro, el objeto y cursos
                dboper.delOneDocument(db, 'alumnos', req.params.idField)
                    .then(() => {
                        client.close();
                        res.end(JSON.stringify({ result: 'ok' }));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    });

//Corresponde a /alumnos/:idField/curso/:idCurso pasandole el id del curso
modelRouter.route('/:idField/curso/:idCurso')
    //Envia a todas las peticiones sea cual sea
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.setHeader('Content-Type', 'applictiona/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    .options((req, res, next) => {
        res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        res.end();
    })
    //Modificara el curso del alumno
    .put((req, res, next) => {
        MongoClient.connect(url, { useNewUrlParser: true })
            .then((client) => {
                const db = client.db(dbname);
                //Objeto donde ponemos lo que vamos a modificar
                let asignaAlumnosCurso = {
                    curso: req.params.idCurso
                }
                //Actualizar el documento
                dboper.updateDocument(db, req.params.idField, asignaAlumnosCurso, 'alumnos')
                    .then((docs) => {
                        client.close();
                        res.end(JSON.stringify(docs));
                    })
                    .catch((err) => { res.end(JSON.stringify(err)) });
            }).catch((err) => { res.end(JSON.stringify(err)) });
    })
    //Borramos todos los alumnos del cursos
    .delete((req, res, next) => {
        //
    });


module.exports = modelRouter;
