const express = require('express');
//Permite ver la url y extraer informacion que llega
const bodyParser = require('body-parser');

//Conjunto de respuestas para una direccion inicial
const modelRouter = express.Router();

//BBDD
const MongoClient = require('mongodb').MongoClient;


//--------- 
const model = 'curso';
const models = 'cursos';
//-----------------------

// modelRouter.use(bodyParser.json());
modelRouter.use(bodyParser.urlencoded({
    extended: true
}));

// Cuando pones /cursos
modelRouter.route('/')
    //Ejecutas esto te pidan lo que pidan, para todos
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/json'); // TEXT PLANER
        next(); // continuara cercant matxos de la ruta
    })
    //Si es get escribes el mensaje
    .get((req, res, next) => {
        // req, res tenen els canvis anteriors si n'hi ha!
        res.end(`Te enviaré todos los ${models}!`);
    })
    .post((req, res, next) => {
        // en el body del req hi ha d'haver info, el body-parser ho posa tot dins req.body
        res.end(`Agregará el ${model}: "${req.body.nombre}" con detalles: "${req.body.descripcion}".`);
    })
    //put es para update
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end(`PUT no soportado en /${models}.`);
    })
    //delete para eliminar
    .delete((req, res, next) => {
        res.end(`Borrando todos los ${models}!`);
    });



modelRouter.route('/:idField')
    .get((req, res, next) => {
        res.end(`Le enviaremos los detalles de ${model}: ${req.params.idField} a usted!`);
    })
    .post((req, res, next) => {
        res.statusCode = 403;
        res.end(`PUT no soportado en /${models}/${req.params.idField} `); // no tÃ© sentit fer un post amb id
    })
    .put((req, res, next) => {
        res.write(`Actualizando el ${model} ${req.params.idField}.\n`);
        res.end(`Actualizará el ${model} "${req.body.nombre}" con detalles "${req.body.descripcion}".`);
    })
    .delete((req, res, next) => {
        res.end(`Borrando el ${model} ${req.params.idField}.`);
    });


module.exports = modelRouter;
