// requeriments
const express = require('express');
const morgan = require('morgan');
const cursoRouter = require('./routes/cursoRouter');
const alumnoRouter = require('./routes/alumnoRouter');

//constants
const hostname = 'localhost';
const port = 3000;

//iniciem app express
const app = express();
app.use(morgan('dev'));

//Siempre que encuentra /cursos vas a cursoRouter
app.use('/cursos', cursoRouter);
app.use('/alumnos', alumnoRouter);

//const server = http.createServer(app);

app.listen(port, hostname, () => {
  console.log(`Servidor corriendo por http://${hostname}:${port}/`);
});