const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017/academy';
const dbname = 'academy';

MongoClient.connect(url, { useNewUrlParser: true })
    //Cuando haya terminado devuelve client
    .then((client) => {
        console.log('Conectado al servidor');

        //Accedemos a la BBDD
        const db = client.db(dbname);
        //Coleccion tempcursos
        const collection = db.collection("tempcursos");

        //Insertamos registros
        collection.insertOne({ "nombre": "Javascript NodeJS", "descripcion": "curso javascript 150h" })
            //En ver de una callback hacemos promesas
            //result= el resultado de la inserccion
            .then((result) => {
                console.log("Despues de insertar:\n");
                console.log(result.ops);
                //Devuelve toda la colleccion de todos los objetos en forma de array
                return collection.find({}).toArray();
            })
            //Lo que devuelve el return anterior se guarda en docs
            .then((docs) => {
                console.log("Encontrado:\n");
                console.log(docs);
                return db.dropCollection("tempcursos");
            })
            .then((result) => {
                console.log("Tancat!");
                client.close();
            })
            .catch((err) => console.log(err));
    //Cuando hayaa un error
    }).catch((err) => console.log(err));