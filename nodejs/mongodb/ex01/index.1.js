const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

const url = 'mongodb://localhost:27017/academy';
const dbname = 'academy';

MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {

    assert.equal(err, null);

    console.log('Conectado al servidor');

    const db = client.db(dbname);
    //Coleccion
    const collection = db.collection("tempcursos");

    //Insertamos en la coleccion. En la callback le pasamos un err y un resultado, si sale bien result, sino err
    collection.insertOne({ "nombre": "Javascript NodeJS", "descripcion": "curso javascript 150h" },
        (err, result) => {
            //Funcion que verifica que err sea igual a null (no haya error)
            //Si no hay error continua, si err no es null se detiene la app
            assert.equal(err, null);

            console.log("Despues de insertar:\n");
            console.log(result.ops);

            //Pedimos todos los datos de la coleccion que definimos arriba
            collection.find({}).toArray((err, docs) => {
                assert.equal(err, null);

                console.log("Encontrado:\n");
                console.log(docs);

                db.dropCollection("tempcursos", (err, result) => {
                    assert.equal(err, null);

                    client.close();
                });
            });
        });

});