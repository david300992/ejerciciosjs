const assert = require('assert');

//db base de datos (academy)
//document documento, dato a guardar
//collection (cursos, alumnos...)
//callback función que se llamará con el dato devuelto
exports.insertDocument = (db, document, collection, callback) => {
    const coll = db.collection(collection);
    // insert inserta el documento y ejecuta la función callback
    // pasándole el posible error y el resultado si ok
    return coll.insert(document);
};

// consulta los documentos (registros) de la colección facilitada
// se devuelven via la callback
exports.findDocuments = (db, collection, callback) => {
    const coll = db.collection(collection);
    return coll.find({}).toArray();
};

// facilitamos el documento a eliminar
exports.removeDocument = (db, document, collection, callback) => {
    const coll = db.collection(collection);
    coll.deleteOne(document);
};

// facilitamos el documento a modificar
// los cambios en update
exports.updateDocument = (db, document, update, collection, callback) => {
    const coll = db.collection(collection);
    return coll.updateOne(document, { $set: update }, null);
};