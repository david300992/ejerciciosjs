const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const dboper = require('./operations');

const url = 'mongodb://localhost:27017/academy';
const dbname = 'academy';

MongoClient.connect(url, {useNewUrlParser: true})
.then((client) => {
        console.log('Conectado al servidor');
        const db = client.db(dbname);
        dboper.insertDocument(db, {
                nombre: "Java+BI",
                descripcion: "Java enfocado a Business Intelligence"
            }, "cursos")
        .then((result) => {
                        console.log("Insertar Documento:\n", result.ops);
                        return dboper.findDocuments(db, "cursos");
                    })

        .then((docs) => {
                console.log("Documentos encontrados:\n", docs);
                return dboper.updateDocument(db, {
                    nombre: "Java+BI"
                }, {
                    descripcion: "Java+BI Java enfocado a BI"
                }, "cursos");
            })

        .then((result) => {
                console.log("Documento actualizado:\n", result.result);
                return dboper.findDocuments(db, "cursos");
            })

        .then((docs) => {
                console.log("Encontrados documentos actualizados:\n", docs);

                return db.dropCollection("cursos");
            })

        .then((result) => {
                console.log("Collection borrada: ", result);

                return client.close();
        })
        .catch((err) => console.log(err));

}).catch((err) => console.log(err));